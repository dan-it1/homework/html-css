const gulp = require("gulp");
const gulpSASS = require('gulp-sass');
const gulpClean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const prefix = require('gulp-autoprefixer');
const minifyJS = require('gulp-js-minify');
const minifyIMG = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

gulp.task('instance:start', (done) => {
    browserSync.init({
        server: {
            baseDir: "../"
        }
    });
    done();
});

gulp.task('instance:reload', (done) => {
    browserSync.reload();                               // релоад не пашет
    done();
});

gulp.task('build:delete', (done) => {
    return gulp.src('../build/*', {read: false})
        .pipe(gulpClean({force: true}));
});

gulp.task('handleCSS', (done) => {
    gulp.src('../build/css/*', {read: false})
        .pipe(gulpClean({force: true}));
    gulp.src("./scss/*.scss")
        .pipe(gulpSASS().on('error', gulpSASS.logError))
        .pipe(concat('styles.min.css'))
        .pipe(prefix())
        .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest("../build/css/"));
    done();
});

gulp.task('handleJS', (done) => {
    gulp.src('../build/scripts/*', {read: false})
        .pipe(gulpClean({force: true}));
    gulp.src("./scripts/*.js")
        .pipe(concat('scripts.min.js'))
        .pipe(minifyJS())
        .pipe(gulp.dest("../build/scripts/"));
    done();
});

gulp.task('handleIMG', (done) => {
    gulp.src('../build/img/*', {read: false})
        .pipe(gulpClean({force: true}));
    gulp.src("./img/*.jpg")
        .pipe(minifyIMG())
        .pipe(gulp.dest("../build/img/"));
    done();
});

gulp.task('handleFiles', gulp.parallel("handleCSS", "handleJS", "handleIMG"));

gulp.task("build:watch", () => {
    gulp.watch("./scss/*.scss", gulp.series("handleCSS", "instance:reload"));
    gulp.watch("./scripts/*.js", gulp.series("handleJS", "instance:reload"));
    gulp.watch("./img/*.jpg", gulp.series("handleIMG", "instance:reload"));
    gulp.watch("../index.html", gulp.series("instance:reload"))
});

gulp.task('build', gulp.series("build:delete", "handleFiles"));
gulp.task('dev', gulp.series("build", "instance:start", "build:watch"));            // вотч не включается